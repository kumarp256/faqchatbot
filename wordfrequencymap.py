from decimal import *

def createWordFrequencyVector(fullSet, inputWords):
    word_vector = []
    sumOfValues = sum(inputWords.values())
    for word in fullSet:
        if word in inputWords:
            word_vector.append(round((Decimal(inputWords[word])/Decimal(sumOfValues)),8))
        else:
            word_vector.append(0)

    return word_vector