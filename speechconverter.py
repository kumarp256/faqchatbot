import speech_recognition as sr
import clipboard

SEMI_COLON_TEXT = 'semicolon'
COMMA_TEXT = 'comma'
COLON_TEXT = 'colon'
ENTER_TEXT = 'enter'
SPACE_TEXT = 'space'
DASH_TEXT = 'dash'
HYPHEN_TEXT = 'hyphen'
EQUALS_TEXT = 'equals'
EQUAL_TO_TEXT = 'equalto'
EQUAL_TO_TEXT_EXT = 'equal to'
PERCENTAGE_TEXT = 'percentage'
TAB_TEXT = 'tab'
PIPE_TEXT = 'pipe'

SEMI_COLON = ';'
COMMA = ','
COLON = ':'
ENTER = '\n'
SPACE = ' '
HYPHEN = '-'
EQUALS = '='
PERCENTAGE = '%'
TAB = '\t'
PIPE = '|'

def start_recorder(writer):
    r = sr.Recognizer()
    with sr.Microphone() as source:  # use the default microphone as the audio source
        r.adjust_for_ambient_noise(source)
        while True:
            audio = r.listen(source)  # listen for the first phrase and extract it into audio data

            try:
                text = r.recognize_google(audio)
                symbolised_text = replace_text_with_symbols_recursively(text)
                old_text = clipboard.paste()
                clipboard.copy(old_text + '\n' + symbolised_text)
                print('Copied => ' + symbolised_text)  # recognize speech using Google Speech Recognition
                writer.write(symbolised_text)
                writer.write('\n')
                writer.flush()
            except LookupError:  # speech is unintelligible
                print("Could not understand audio")


def replace_text_with_symbol(text):
    if SEMI_COLON_TEXT in text.strip():
        return text.replace(SEMI_COLON_TEXT, SEMI_COLON)
    elif COMMA_TEXT in text.strip():
        return text.replace(COMMA_TEXT, COMMA)
    elif COLON_TEXT in text.strip():
        return text.replace(COLON_TEXT, COLON)
    elif ENTER_TEXT in text.strip():
        return text.replace(ENTER_TEXT, ENTER)
    elif SPACE_TEXT in text.strip():
        return text.replace(SPACE_TEXT, SPACE)
    elif DASH_TEXT in text.strip():
        return text.replace(DASH_TEXT, HYPHEN)
    elif HYPHEN_TEXT in text.strip():
        return text.replace(HYPHEN_TEXT, HYPHEN)
    elif EQUAL_TO_TEXT in text.strip():
        return text.replace(EQUAL_TO_TEXT, EQUALS)
    elif EQUALS_TEXT in text.strip():
        return text.replace(EQUALS_TEXT, EQUALS)
    elif EQUAL_TO_TEXT_EXT in text.strip():
        return text.replace(EQUAL_TO_TEXT_EXT, EQUALS)
    elif PERCENTAGE_TEXT in text.strip():
        return text.replace(PERCENTAGE_TEXT, PERCENTAGE)
    elif TAB_TEXT in text.strip():
        return text.replace(TAB_TEXT, TAB)
    elif PIPE_TEXT in text.strip():
        return text.replace(PIPE_TEXT, PIPE)
    else:
        return text


def replace_text_with_symbols_recursively(read_text):
    text = replace_text_with_symbol(read_text)
    if EQUAL_TO_TEXT_EXT in text or SEMI_COLON_TEXT in text or COMMA_TEXT in text or COLON_TEXT in text or ENTER_TEXT in text or SPACE_TEXT in text or DASH_TEXT in text or HYPHEN_TEXT in text or EQUALS_TEXT in text or EQUAL_TO_TEXT in text or PERCENTAGE_TEXT in text:
        return replace_text_with_symbols_recursively(text)
    else:
        return text


if __name__ == '__main__':
    # x = 'ritesh comma is a dash boy'
    # print replace_text_with_symbols_recursively(x)

    file_to_write = 'output.txt'

    writer = open(file_to_write, 'a')
    start_recorder(writer)
