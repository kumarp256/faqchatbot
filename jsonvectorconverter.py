import json
import numpy as np

from pprint import pprint
from nltk import word_tokenize
from nltk.corpus import stopwords
from string import punctuation

from wordfrequencymap import createWordFrequencyVector

if __name__ == '__main__':
    with open('faq_hdfc.json') as data_file:
        data = json.load(data_file)

    print("/------------- START -------------/")
    # print the keys and values
    stop = set(stopwords.words('english') + list(punctuation))
    unique_words = set()
    sentence_vector = []
    # create dictionary of index and answer since we are appending questions in the vector
    # so index should be retained when we try to retrieve value later based on index associated with question
    index_answer_map = {}
    counter = 0
    for key in data:
        value = data[key]

        # tokenize key and count frequency
        refWordFreq = {}
        for i in word_tokenize(key.lower()):
            if i not in stop:
                unique_words.add(i)
                refWordFreq[i] = refWordFreq.setdefault(i, 0) + 1

        sentence_vector.append(refWordFreq)
        index_answer_map[counter] = value
        counter += 1

    print("/------------- Distinct Words -------------/")
    # pprint(unique_words)
    print("/------------- Unique Words Count -------------/")
    # print(len(unique_words))
    print("/------------- Sentence Vector -------------/")
    # Create matrix of all sentences
    ref_matrix = []
    for curr_sentence in sentence_vector:
        ref_matrix.append(createWordFrequencyVector(unique_words, curr_sentence))

 #   pprint(ref_matrix)

    print("/------------- Test any sentence -------------/")
    # input_question = "Can I have a Super Saver Facility and a Sweep-in Facility on the one Fixed Deposit?"
    input_question = raw_input("Enter your query: ")
    input_tokenized = [i for i in word_tokenize(input_question.lower()) if i not in stop]
    wordFreqVect = {}
    for word in input_tokenized:
        wordFreqVect[word] = wordFreqVect.setdefault(word, 0) + 1

    input_sentence_vector = createWordFrequencyVector(unique_words, wordFreqVect)
    # pprint(input_sentence_vector)
    ref_matrix_len = len(ref_matrix)
    input_padded_matrix = np.array([input_sentence_vector, ] * ref_matrix_len)
    # pprint(input_padded_matrix)

    np_ref_matrix = np.array(ref_matrix)
    sse = np.array((input_padded_matrix - np_ref_matrix) ** 2)
    sse = np.sum(sse, axis=1)
    # pprint(sse)

    idx = np.where(sse == sse.min())[0]
    # print("min_val = {0}".format(idx[0]))
    print("/------------- ANSWER -------------/")
    answer = index_answer_map[idx[0]]
    pprint(answer)
    print("/------------- END -------------/")
