import re
import json


def sanitize_line(q):
    return q.replace('\t', '').replace('\n', '').replace('\xe2\x80\xa2', '')


if __name__ == '__main__':

    with open('/home/ritesh/jusbots/faq_hdfc.txt', 'r') as reader:

        faq_dict = {}
        curr_q = None
        prev_q = None
        for line in reader:
            if not re.match(r'^\s*$', line):
                # Its a question
                if line[0].isdigit():
                    prev_q = curr_q
                    curr_q = ''.join(sanitize_line(line)[2:]).replace('.', '')
                    faq_dict[curr_q] = []
                else:
                    if curr_q in faq_dict.keys():
                        answers = faq_dict[curr_q]
                        sanitised_ans = sanitize_line(line)
                        answers.append(sanitised_ans)
                        faq_dict[curr_q] = answers
                    else:
                        print 'Question not added yet !!!'

        # TODO - remove empty keys/value
        faq_dict_final = {k: v for k, v in faq_dict.items() if len(v) > 0}

        with open('/home/ritesh/jusbots/faq_hdfc.json', 'w') as writer:
            writer.write(json.dumps(faq_dict_final))

        print ('faq_dict is %s', json.dumps(faq_dict_final))
        print ('Json written successfully at the same location')
